LothsArbalest/Fire     "Sounds/LothsArbalestFire.flac"
LothsArbalest/Reload   "Sounds/LothsArbalestReload.flac"
LothsArbalest/Flyby    "Sounds/LothsArbalestFlyby.flac"
LothsArbalest/Icing    "Sounds/LothsArbalestIcing.flac"
LothsArbalest/Hit      "Sounds/LothsArbalestHit.flac"

$limit LothsArbalest/Icing 0
$limit LothsArbalest/Hit 0