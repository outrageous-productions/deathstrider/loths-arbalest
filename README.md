# Loth's Arbalest

_(or 'Arbalest of Loth' because GitLab doesn't allow apostrophes in project names)_

A weapon addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

A battery-powered cryo crossbow, simple as that.

PillowBlaster: I always liked that gun, if me nicking it from Realm667 ages ago speaks any, but never saw a good place to use it. Since it feels like it would fit Deathstrider like a glove, I thought "Ah, finally!". So, here it is, with some tweaks by yours truly.

---

See [credits.txt](./credits.txt) for attributions.
